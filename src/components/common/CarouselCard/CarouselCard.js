import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

function CarouselCard({ data, cart, settings }) {
  return (
    <Slider {...settings}>
      {data.map((item) => (
        <div key={item.id}>{cart(item)}</div>
      ))}
    </Slider>
  );
}

export { CarouselCard };
